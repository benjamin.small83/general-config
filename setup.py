#! /usr/bin/env python3.6

from setuptools import setup, find_packages

setup(
    name="general-config",
    packages=find_packages(),
    py_modules=["general_config"],
    version='0.0.1',
    description=('A quick module for pulling configs from standard locations, environments and '
                 'in secrets for docker and kubernetes images'),
    author='Ben Small',
    author_email='benjamin.small83@gmail.com',
    url='https://gitlab.com/benjamin.small83/general-config',
    python_requires=">=3.7"
)